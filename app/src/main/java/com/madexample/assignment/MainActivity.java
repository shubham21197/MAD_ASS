package com.madexample.assignment;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    EditText q1et1, q1et2,q4et1,q4et2,q4et3,q4et4;
    Switch q2sw;
    String list[] = {"Newcastle", "Chelsea", "Liverpool", "Southampton", "Manchester City", "Manchester United"};
    String curTeam = "";
    String[] states = {"UP","AP","Karnataka","Delhi","Chennai","Bihar"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Question 1

        q1et1 = (EditText) findViewById(R.id.q1et1);
        q1et2 = (EditText) findViewById(R.id.q1et2);

        findViewById(R.id.q1b1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                q1et2.setText(q1et1.getText());
            }
        });

        findViewById(R.id.q1b2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                q1et2.setText("");
                q1et1.setText("");
            }
        });

        //Question 2

        q2sw = (Switch) findViewById(R.id.q2sw);

        findViewById(R.id.q2b).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int secs;
                secs = Calendar.getInstance().get(Calendar.SECOND);
                Toast.makeText(MainActivity.this, "" + secs, Toast.LENGTH_SHORT).show();
                if (secs <= 30)
                    q2sw.setChecked(true);
                else
                    q2sw.setChecked(false);
            }
        });

        //Question 3

        findViewById(R.id.q3b).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText et1, et2;
                CheckBox cb1, cb2;
                et1 = (EditText) findViewById(R.id.q3et1);
                et2 = (EditText) findViewById(R.id.q3et2);
                RadioGroup rg = (RadioGroup) findViewById(R.id.q3rgrp);
                RadioButton rb = (RadioButton) findViewById(rg.getCheckedRadioButtonId());
                String gender = rb.getText().toString();
                String name = et1.getText().toString();
                String mail = et2.getText().toString();
                cb1 = (CheckBox) findViewById(R.id.q3ch1);
                cb2 = (CheckBox) findViewById(R.id.q3ch2);
                String str = "Name: " + name + " Mail: " + mail + " Gender: " + gender;
                if (cb1.isChecked()) {
                    str += " Need Book ";
                }
                if (cb2.isChecked()) {
                    str += " Need Notebook";
                }

                Toast.makeText(MainActivity.this, str, Toast.LENGTH_SHORT).show();

            }
        });

        final ArrayList<String> alist = new ArrayList<String>(Arrays.asList(list));
        final ArrayAdapter adapter = new ArrayAdapter<String>(this,
                R.layout.list_adapter, alist);

        ListView listView = (ListView) findViewById(R.id.lview);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String temp = alist.get(position);
                if (temp.equals(curTeam)) {
                    if (position == (list.length-1)) {
                    } else {
                        alist.remove(position);
                        alist.add(position + 1, temp);
                    }
                } else {
                    curTeam = temp;
                    alist.remove(position);
                    alist.add(0, temp);
                }
                adapter.notifyDataSetChanged();
            }
        });

        findViewById(R.id.q5b).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, alist.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        //Question4

        q4et1=(EditText)findViewById(R.id.q4et1);
        q4et2=(EditText)findViewById(R.id.q4et2);
        q4et3=(EditText)findViewById(R.id.q4et3);
        q4et4=(EditText)findViewById(R.id.q4et4);

        ArrayAdapter<String> state_adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,states);
        state_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        final Spinner spinner = (Spinner)findViewById(R.id.spinner);
        spinner.setAdapter(state_adapter);
        findViewById(R.id.q4b).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String txt = q4et1.getText().toString()+" "+q4et2.getText().toString()+" "+q4et3.getText().toString()+" "+q4et4.getText().toString()+" "+spinner.getSelectedItem().toString();
                Toast.makeText(MainActivity.this, txt, Toast.LENGTH_SHORT).show();
            }
        });


    }

}
